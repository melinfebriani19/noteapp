package com.papb.tugasakhir.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.papb.tugasakhir.R;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private EditText inEmail, inPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        inEmail = findViewById(R.id.et_email);
        inPassword = findViewById(R.id.et_password);

        TextView tvlog = findViewById(R.id.btn_login);
        tvlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToLogin = new Intent(RegisterActivity.this, LoginActivity.class);
                goToLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                startActivity(goToLogin);
                finish();
            }
        });

        Button btnReg = findViewById(R.id.btn_register);
        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reqRegister();
            }
        });
    }
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }
    private void reqRegister() {
        //AMBIL DATA DARI TEXT INPUT
        String email = inEmail.getText().toString();
        String password = inPassword.getText().toString();

        //CEK DATANYA APAKAH ADA ISI
        if (email == null) {
            Toast.makeText(RegisterActivity.this, "Email Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (password == null) {
            Toast.makeText(RegisterActivity.this, "Password Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            //REGISTER AKUN BARU
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                //SAAT REGISTER BERHASIL
                                Toast.makeText(RegisterActivity.this, "Akun Berhasil Dibuat", Toast.LENGTH_SHORT).show();

                                Intent goHome = new Intent(RegisterActivity.this, MainActivity.class);
                                goHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                startActivity(goHome);
                                RegisterActivity.this.finish();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Kesalahan Dalam Membuat Akun : " + task.getException(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }
}