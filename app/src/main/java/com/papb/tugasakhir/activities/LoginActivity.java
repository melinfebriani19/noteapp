package com.papb.tugasakhir.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.papb.tugasakhir.R;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private EditText inEmail, inPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        inEmail = findViewById(R.id.et_email);
        inPassword = findViewById(R.id.et_password);

        TextView tvReg = findViewById(R.id.btn_register);
        tvReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goRegister = new Intent(LoginActivity.this, RegisterActivity.class);
                goRegister.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                startActivity(goRegister);
                finish();
            }
        });

        Button btnLog = findViewById(R.id.btn_login);
        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reqLogin();
            }
        });
    }
    private void reqLogin() {
        //AMBIL DATA DARI TEXT INPUT
        String email = inEmail.getText().toString();
        String password = inPassword.getText().toString();

        //CEK DATANYA APAKAH ADA ISI
        if (email == null) {
            Toast.makeText(LoginActivity.this, "Email Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else if (password == null) {
            Toast.makeText(LoginActivity.this, "Password Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        } else {
            //REGISTER AKUN BARU
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                //SAAT REGISTER BERHASIL
                                Toast.makeText(LoginActivity.this, "Login Berhasil", Toast.LENGTH_SHORT).show();

                                Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);
                                mainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                                startActivity(mainActivity);
                                LoginActivity.this.finish();
                            } else {
                                Toast.makeText(LoginActivity.this, "Kesalahan Saat Login : " + task.getException(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }
}